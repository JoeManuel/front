import React, {useEffect, useState} from "react";
import {useNavigate} from 'react-router-dom';
// import "./css/laboratorio.css";

import Navbar from "./Navbar";

const Home = () => {
  const navigate = useNavigate();
  const [laboratorios, setLaboratorio] = useState([]);

  const loadLaboratorio = async () => {
    const response = await fetch("http://localhost:4000/lab");
    const data = await response.json();
    setLaboratorio(data);
  };

  useEffect(() => {
    loadLaboratorio();
  }, []);
  return (
    <>
      <Navbar/>
      <div className="lista-laboratorio" >
      <div className="ejemplo-list">
          <h1>Laboratorios</h1>
          {laboratorios.map((laboratorio) => (
            <div
              className="card-laboratorio-list"
              key={laboratorio.id_laboratorio}
            >
              <div className="card-laboratorio-content">
                <div>
                  <div>
                    {" "}
                    <strong>Nombre:</strong> {laboratorio.nombre_laboratorio}{" "}
                  </div>
                  <div>
                    {" "}
                    <strong>Numero de maquinas:</strong>{" "}
                    {laboratorio.numero_maquinas}{" "}
                  </div>
                  <div> {laboratorio.estado_laboratorio} </div>
                </div>
                <div className="btn-container">
                  {/* <button
                    className="btn-edit"
                    onClick={() =>
                      navigate(`/lab/${laboratorio.id_laboratorio}/edit`)
                    }
                  >
                    {" "}
                    Editar
                  </button>
                  <button
                    className="btn-delete"
                    onClick={() => handleDelete(laboratorio.id_laboratorio)}
                  >
                    Eliminar
                  </button> */}
                </div>
              </div>
            </div>
          ))}
        </div>
        </div>
      
    </>

  )
};

export default Home;
