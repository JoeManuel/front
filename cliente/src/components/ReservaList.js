import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./css/reserva.css";
import Navbar from "./Navbar";

const ReservaList = () => {
  // const [editing, setEditing] = useState(false);
  const navigate = useNavigate();
  const [reservas, setReservas] = useState([]);

  const loadReserva = async () => {
    const response = await fetch("http://localhost:4000/reserva");
    const data = await response.json();
    setReservas(data);
  };

  //eliinar una reserva
  const handleDelete = async (id_reservar_laboratorio) => {
    try {
      await fetch(`http://localhost:4000/reserva/${id_reservar_laboratorio}`, {
        method: "DELETE",
      });
      alert("Reserva eliminada con éxito");
      setReservas(
        reservas.filter(
          (reservar_laboratorio) =>
            reservar_laboratorio.id_reservar_laboratorio !==
            id_reservar_laboratorio
        )
      );
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    loadReserva();
  }, []);

  return (
    <>
      <Navbar />
      <div className="lista-reserva">
        <div className="crear-reserva">
          <button
            className="btn-new-reserva"
            onClick={() => navigate("/reserva/new")}
          >
            Nueva Reserva
          </button>
        </div>
        <div className="reserva-list">
          <h1>Resevas de Laboratorios</h1>
          {reservas.map((reservar_laboratorio) => (
            <div
              className="card-reserva-list"
              key={reservar_laboratorio.id_reservar_laboratorio}
            >
              <div className="card-reserva-content">
                <div>
                  <div>
                    <strong>Numero de maquinas a usar:</strong>{" "}
                    {reservar_laboratorio.numero_maquinas_usar}
                  </div>
                  <div>
                    <strong>Fecha de Reserva:</strong>{" "}
                    {reservar_laboratorio.fecha_reserva}
                  </div>
                  <div>
                    <strong>Hora Inicio:</strong>{" "}
                    {reservar_laboratorio.hora_inicio}
                  </div>
                  <div>
                    <strong>Hora Fin:</strong>{" "}
                    {reservar_laboratorio.hora_fin}
                  </div>
                </div>
                  <div className="btn-container-reserva" >
                  <button
                    className="btn-edit"
                    onClick={() =>
                      navigate(`/reserva/${reservar_laboratorio.id_reservar_laboratorio}/edit`)
                    }
                  >
                    {" "}
                    Editar
                  </button>
                  <button
                    className="btn-delete"
                    onClick={() => handleDelete(reservar_laboratorio.id_reservar_laboratorio)}
                  >
                    Eliminar
                  </button>
                  <button
                    className="btn-observacion"
                    onClick={() => 
                      navigate(`/observacion`)}
                  >
                    Observacion
                  </button>
                  </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default ReservaList;
