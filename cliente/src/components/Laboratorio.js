import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import "./css/laboratorio.css";
import Navbar from "./Navbar";

const Laboratorio = () => {
  //useState
  const [laboratorio, setLaboratorio] = useState({
    nombre_laboratorio: "",
    numero_maquinas: "",
    estado_laborartorio: "false",
  });

  const [errores, setErrores] = useState({});
  //defi9niendo el setLoading
  const [loading, setLoading] = useState(false);
  //definiendo el estado para editar el setLoading
  const [editing, setEditing] = useState(false);
  //definienfo el use navigate
  const navigate = useNavigate();
  const params = useParams();

  // eventos de la captura de datos
  const handleSubmit = async (e) => {
    e.preventDefault();
    //establecemos el loading
    setLoading(true);

    if (editing) {
      await fetch(`http://localhost:4000/lab/${params.id_laboratorio}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(laboratorio),
      });
    } else {
      await fetch("http://localhost:4000/lab", {
        method: "POST",
        body: JSON.stringify(laboratorio),
        headers: { "Content-Type": "application/json" },
      });
    }

    // console.log(data);
    setLoading(false);

    if (editing) {
      alert("laboratorio editado con exito !!!");
    } else {
      alert("laboratorio creado con exito!!!");
    }

    navigate("/lab");
  };

  const handleChange = (e) => {
    setLaboratorio({ ...laboratorio, [e.target.name]: e.target.value });
  };

  const loadLaboratorio = async (id_laboratorio) => {
    const res = await fetch(`http://localhost:4000/lab/${id_laboratorio}`);
    const data = await res.json();
    setLaboratorio({
      nombre_laboratorio: data.nombre_laboratorio,
      numero_maquinas: data.numero_maquinas,
      estado_laborartorio: data.estado_laborartorio,
    });
    setEditing(true);
  };
  useEffect(() => {
    if (params.id_laboratorio) {
      loadLaboratorio(params.id_laboratorio);
    }
  }, [params.id_laboratorio]);

  
  const validateForm = (laboratorio) => {

    let errores = {};

    let nombre = /^[a-zA-ZÀ-ÿ\s0-9]{2,100}$/; // Letras y espacios, pueden llevar acentos.

    if (laboratorio.nombre_laboratorio == "") {
      errores.nombre_laboratorio = "requiere completar el campo"
    } else if (nombre.test(laboratorio.nombre_laboratorio) === false) {
      errores.nombre_laboratorio = "solo letras y espacios, pueden llevar acentos y numeros"
    }
    if(laboratorio.numero_maquinas === ""){
      errores.numero_maquinas = "campo requerido"
    }else if(parseInt(laboratorio.numero_maquinas, 10) < 10){
      errores.numero_maquinas = " no menor a 10 maquinas"
    }else if(parseInt(laboratorio.numero_maquinas, 10) > 30){
      errores.numero_maquinas = " no mayor a 30 maquinas"
    }

    return errores;
  };

  const handleBlur = (e) => {
    handleChange(e);
    setErrores(validateForm(laboratorio))
  };

  return (
    <>
      <Navbar />

      <div>
        <div className="container-laboratorio">
          <h1>
            {" "}
            {editing ? "Editar laboratorio" : "Crear un nuevo laboratorio"}
          </h1>
          <form className="laboratorio" onSubmit={handleSubmit}>
            <div className="date-laboratorio">
              <input
                type="text"
                required
                onChange={handleChange}
                onBlur={handleBlur}
                name="nombre_laboratorio"
                // value={laboratorio.nombre_laboratorio}
              />
              <span></span>
              <label>Nombre de la laboratorio</label>
            </div>
            {errores.nombre_laboratorio && <p className='error'>{errores.nombre_laboratorio}</p>}

            <div className="date-laboratorio">
              <input
                type="number"
                min={1}
                required
                onChange={handleChange}
                onBlur={handleBlur}
                name="numero_maquinas"
                // value={laboratorio.numero_maquinas}
              />
              <span></span>
              <label>Numero de maquinas</label>
            </div>
            {errores.numero_maquinas && <p className='error'>{errores.numero_maquinas}</p>}


            <input
              className="btn-guardar-laboratorio"
              type="submit"
              value={editing ? "actualizar" : "crear"}
            />
          </form>
        </div>
      </div>
    </>
  );
};

export default Laboratorio;
