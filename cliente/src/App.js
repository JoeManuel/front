import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./components/Login";
import Registrarse from "./components/Registrarse";
// import Navbar from "./components/Navbar";
import Home from "./components/Home";
import Materia from "./components/Materia";
import MateriaList from "./components/MateriaList";
import Laboratorio from "./components/Laboratorio";
import LaboratorioList from "./components/LaboratorioList";
import Reserva from "./components/Reserva";
import ReservaList from "./components/ReservaList";
import Observacion from "./components/Observacion"
import ObservacionList from "./components/ObservacionList"
// import { useState } from "react";

function App() {

  // const [logged, setLogged] = useState(false);

  return (
    <div className="App">
      <BrowserRouter>
        {/* <Navbar /> */}
        
        <Routes>
          <Route path="/" element={<Login />} />
          
          <Route path="/registrate" element={ <Registrarse />} />
          <Route path="/home" element={<Home />} />          
          <Route path="/materia/new" element={<Materia />} />
          <Route path="/materia" element={<MateriaList />} />
          <Route path="/materia/:id_materia/edit" element={<Materia />} />
          <Route path="/lab/new" element={<Laboratorio />} />
          <Route path="/lab" element={<LaboratorioList />} />
          <Route path="/lab/:id_laboratorio/edit" element={<Laboratorio />} />
          <Route path="/reserva/new" element={<Reserva />} />
          <Route path="/reserva" element={<ReservaList />} />
          <Route path="/reserva/:id_reservar_laboratorio/edit" element={<Reserva />} />
          <Route path="/observacion/new" element={<Observacion />} />
          <Route path="/observacion" element={<ObservacionList />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
